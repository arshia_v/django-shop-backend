from django.urls import path
from . import views
urlpatterns = [
    path('', views.AdminUserPanelView.as_view(), name='user_panel_page'),
    path('edit-profile', views.EditUserProfileView.as_view(), name='edit_profile_page'),
    path('chang-pass', views.ChangePasswordPanelView.as_view(), name='change_password_profile_page'),
    path('user-basket', views.user_baskets, name='user_basket_page'),
    path('remove-order-detail', views.remove_order_detail, name='remove_basket_detail_page'),
    path('change-order-detail', views.change_order_detail_count, name='change_basket_detail_page'),
]