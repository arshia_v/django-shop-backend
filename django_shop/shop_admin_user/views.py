from django.contrib.auth import logout
from django.contrib.auth.decorators import login_required
from django.http import HttpRequest, JsonResponse
from django.shortcuts import render, redirect
from django.template.loader import render_to_string
from django.urls import reverse
from django.views.generic import TemplateView, View

from shop_Admin.models import User
from shop_order.models import Order, OrderDetail
from .forms import AdminUserEditingForm, ChangePasswordForm
from django.utils.decorators import method_decorator

@method_decorator(login_required, name='dispatch')
class AdminUserPanelView(TemplateView):
    template_name = 'admin_user/user_panel_dashboard_page.html'


@method_decorator(login_required, name='dispatch')
class EditUserProfileView(View):
    def get(self, request: HttpRequest):
        current_user = User.objects.filter(id=request.user.id).first()
        form = AdminUserEditingForm(instance=current_user)
        context = {
            'form': form,
            'current_user':current_user
        }
        return render(request, 'admin_user/edit_profile_page.html', context)

    def post(self, request: HttpRequest):
        current_user = User.objects.filter(id=request.user.id).first()
        form = AdminUserEditingForm(request.POST , request.FILES, instance=current_user)
        if form.is_valid():
            form.save(commit=True)

        context = {
            'form': form,
            'current_user': current_user
        }
        return render(request, 'admin_user/edit_profile_page.html', context)

@method_decorator(login_required, name='dispatch')
class ChangePasswordPanelView(View):
    def get(self, request: HttpRequest):
        context = {
            'form': ChangePasswordForm()
        }
        return render(request, 'admin_user/change_password_page.html', context)

    def post(self, request: HttpRequest):
        form = ChangePasswordForm(request.POST)
        current_user = User.objects.filter(id=request.user.id).first()
        if form.is_valid():
            check_password = current_user.check_password(form.cleaned_data.get("current_password"))
            if check_password :
                new_pass = form.cleaned_data.get('new_password')
                current_user.set_password(new_pass)
                current_user.save()
                logout(request)
                return redirect(reverse('login_page'))
            else:
                form.add_error('current_password', 'current password is wrong !!!')
        context = {
            'form':form
        }
        return render(request, 'admin_user/change_password_page.html', context)

@login_required()
def admin_user_panel_component(request):
    return render(request, 'admin_user/components/user_panel_menu_component.html', {})

@login_required()
def user_baskets(request):
    current_order, create = Order.objects.prefetch_related('orderdetail_set').get_or_create(is_paid=False, user_id=request.user.id)
    total_amount = 0
    for order_detail in current_order.orderdetail_set.all():
        total_amount += order_detail.product.price * order_detail.count

    context = {
        'order': current_order,
        'sum': total_amount
    }
    return render(request, 'admin_user/user_basket.html', context)

@login_required()
def remove_order_detail(request):
    detail_id= request.GET.get('detail_id')
    if detail_id is None:
        return JsonResponse({
            'status':'detail_id_not_found'
        })

    current_order, create = Order.objects.prefetch_related('orderdetail_set').get_or_create(is_paid=False,user_id=request.user.id)

    detail = current_order.orderdetail_set.filter(id=detail_id).first()
    if detail is None:
        return JsonResponse({
            'status': 'detail_npt_found'
        })
    detail.delete()

    current_order, create = Order.objects.prefetch_related('orderdetail_set').get_or_create(is_paid=False,user_id=request.user.id)
    total_amount = 0
    for order_detail in current_order.orderdetail_set.all():
        total_amount += order_detail.product.price * order_detail.count

    context = {
        'order': current_order,
        'sum': total_amount
    }
    data = render_to_string('admin_user/user_basket_content.html',context)
    return JsonResponse({
        'status': 'success',
        'body': data
    })

@login_required()
def change_order_detail_count(request: HttpRequest):
    detail_id = request.GET.get('detail_id')
    state = request.GET.get('state')
    if detail_id is None or state is None:
        return JsonResponse({
            'state':'not_found_detail_or_state'
        })
    order_detail = OrderDetail.objects.filter(id=detail_id, order__user_id=request.user.id, order__is_paid=False).first()

    if order_detail is None:
        return JsonResponse({
            'state': 'detail_not_found'
        })

    if state == 'increase':
        order_detail.count += 1
        order_detail.save()

    elif state == 'decrease':
        if order_detail.count == 1 :
            order_detail.delete()
        else:
            order_detail.count -= 1
            order_detail.save()
    else:
        return JsonResponse({
            'state': 'state_invalid'
        })

    current_order, create = Order.objects.prefetch_related('orderdetail_set').get_or_create(is_paid=False,
                                                                                            user_id=request.user.id)
    total_amount = 0
    for order_detail in current_order.orderdetail_set.all():
        total_amount += order_detail.product.price * order_detail.count

    context = {
        'order': current_order,
        'sum': total_amount
    }
    data = render_to_string('admin_user/user_basket.html', context)
    return JsonResponse({
        'status': 'success',
        'body': data
    })