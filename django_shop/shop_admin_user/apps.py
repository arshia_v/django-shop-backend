from django.apps import AppConfig


class ShopAdminUserConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'shop_admin_user'
