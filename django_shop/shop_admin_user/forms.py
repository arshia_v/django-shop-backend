from django import forms
from shop_Admin.models import User


class AdminUserEditingForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'about_user', 'address', 'avatar' ]
        widgets = {
            'first_name' : forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'first name'
            }),
            'last_name': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'last name'
            }),
            'about_user': forms.Textarea(attrs={
                'class': 'form-control',
                'placeholder': 'about user'
            }),
            'address': forms.Textarea(attrs={
                'class': 'form-control',
                'placeholder': 'address '
            }),
            'avatar': forms.FileInput(attrs={

            }),
        }


class ChangePasswordForm(forms.Form):
    current_password = forms.CharField(
        widget=forms.PasswordInput(
            attrs={
                'class':'form-control'
            }))

    new_password = forms.CharField(
        widget=forms.PasswordInput(
            attrs={
                'class': 'form-control'
            }))

    replay_new_password = forms.CharField(
        widget=forms.PasswordInput(
            attrs={
                'class': 'form-control'
            }))

    def clean_replay_new_password(self):
        new_pass = self.cleaned_data.get('new_password')
        rep_pass = self.cleaned_data.get('replay_new_password')

        if new_pass != rep_pass:
            raise forms.ValidationError('the password is not matching')
        return new_pass