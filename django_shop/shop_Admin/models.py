from django.contrib.auth.models import AbstractUser
from django.db import models


class User(AbstractUser):
    avatar = models.ImageField(upload_to='images/user/avatar', null=True, blank=True)
    about_user = models.TextField(null=True, blank=True)
    email_active_code = models.CharField(max_length=200, null=True, blank=True)
    address = models.TextField(null=True, blank=True)
    is_forgot_pass = models.BooleanField(default=False, null=True, blank=True)
