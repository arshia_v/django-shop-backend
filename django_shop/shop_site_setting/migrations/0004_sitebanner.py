# Generated by Django 3.2 on 2022-09-15 20:48

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop_site_setting', '0003_slider'),
    ]

    operations = [
        migrations.CreateModel(
            name='SiteBanner',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=200)),
                ('url_title', models.CharField(max_length=200)),
                ('image', models.ImageField(blank=True, null=True, upload_to='images/banners')),
                ('is_acitve', models.BooleanField()),
                ('position', models.CharField(choices=[('prodcut_list', 'Product List'), ('product_detail', 'Product Detail'), ('product_us', 'Product Us')], max_length=200)),
            ],
        ),
    ]
