from django.apps import AppConfig


class ShopSiteSettingConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'shop_site_setting'
