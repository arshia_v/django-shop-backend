from distutils.command.upload import upload
from tkinter.tix import Tree
from django.db import models

# Create your models here.



class SiteSettings(models.Model):
    site_name = models.CharField(max_length=200)
    site_url = models.CharField(max_length=200)
    address = models.CharField(max_length=400)
    phone = models.CharField(max_length=200, null=True, blank=True)
    fax = models.CharField(max_length=200, null=True, blank=True)
    email = models.CharField(max_length=200, null=True, blank=True)
    copy_right = models.TextField()
    about_us_text = models.TextField()
    site_logo = models.ImageField(upload_to='images/site_settings/')
    main_site_setting =models.BooleanField(default=False)

    def __str__(self):
        return self.site_name



class FooterLinkBox(models.Model):
    title = models.CharField(max_length=200)

    def __str__(self):
        return self.title


class FooterLink(models.Model):
    title = models.CharField(max_length=200)
    url = models.URLField(max_length=500)
    footer_link_box = models.ForeignKey(to=FooterLinkBox, on_delete=models.CASCADE)

    def __str__(self):
        return self.title


class Slider(models.Model):
    slider_name = models.CharField(max_length=200)
    slider_description = models.TextField()
    slider_url_title = models.CharField(max_length=300)
    slider_url = models.URLField(db_index=True)
    slider_image = models.ImageField(upload_to='images/slider', )
    slider_active = models.BooleanField(default=False)

    def __str__(self):
        return self.slider_name


class SiteBanner(models.Model):
    class SiteBannerPositions(models.TextChoices):
        product_list = 'prodcut_list' , 'Product List Page'
        product_detail = 'product_detail', 'Product Detail Page'
        about_us = 'product_us', 'Product Us Page'

    title = models.CharField(max_length=200)
    url_title = models.CharField(max_length=200)
    image = models.ImageField(upload_to='images/banners', null=True, blank=True)
    is_acitve = models.BooleanField()
    position = models.CharField(max_length=200, choices=SiteBannerPositions.choices)

    def __str__(self):
        return self.title