from django.contrib import admin
from .models import SiteSettings, FooterLink,FooterLinkBox, Slider, SiteBanner


class FooterAdmin(admin.ModelAdmin):
    list_display = ['title', 'url']

admin.site.register(SiteSettings)

admin.site.register(FooterLink, FooterAdmin)

admin.site.register(FooterLinkBox)

admin.site.register(Slider)

admin.site.register(SiteBanner)

