from django.db import models
from django.utils.text import slugify
from shop_Admin.models import User


class ProductCategory(models.Model):
    title = models.CharField(max_length=200)
    url_title = models.CharField(max_length=200)
    is_active = models.BooleanField(default=True)
    is_delete = models.BooleanField(default=False)

    def __str__(self):
        return self.title


class ProductBrand(models.Model):
    title = models.CharField(max_length=200)
    url_title = models.CharField(max_length=200)
    is_active = models.BooleanField(default=True)
    is_delete = models.BooleanField(default=False)

    def __str__(self):
        return self.title


class Product(models.Model):
    title = models.CharField(max_length=200)
    price = models.IntegerField()

    category = models.ManyToManyField(ProductCategory, null=True, blank=True, related_name='product_categories')
    brand = models.ForeignKey(ProductBrand, null=True, blank=True, on_delete=models.CASCADE)

    short_description = models.TextField(max_length=300)
    description = models.TextField()
    slug = models.SlugField(null=True, blank=True)
    image = models.ImageField(upload_to='images')
    count_of_ordered = models.IntegerField(default=1)

    is_active = models.BooleanField(default=False)
    is_delete = models.BooleanField(default=False)


    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        super(Product, self).save()


class ProductVisit(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    ip = models.CharField(max_length=30)
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        return f'{self.product.title} / {self.ip}'


class ProductGallery(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    image = models.ImageField(upload_to='images/product-gallery')

    def __str__(self):
        return  self.product.title
