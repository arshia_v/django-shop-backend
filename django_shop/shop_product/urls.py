from django.urls import path
from .views import  ShopListView, ShopDetailView


urlpatterns = [
    path('', ShopListView.as_view(), name='product'),
    path('category/<cat>', ShopListView.as_view(), name='product_category_page'),
    path('brand/<brand>', ShopListView.as_view(), name='product_brand_page'),
    path('<slug:slug>', ShopDetailView.as_view(), name='product_detail'),
]