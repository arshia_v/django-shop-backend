from django.shortcuts import render
from django.views.generic import ListView, DetailView
from shop_site_setting.models import SiteBanner
from .models import Product, ProductCategory, ProductBrand, ProductVisit, ProductGallery
from django.http import HttpRequest
from django.db.models import Count
from utils.http_service import get_client_ip
from utils.convertors import group_list

class ShopListView(ListView):
    template_name = 'product/shop.html'
    model = Product
    context_object_name = 'products'
    ordering = ["-price"]
    paginate_by = 6


    def get_queryset(self):
        query = super(ShopListView, self).get_queryset()
        query = query.filter(is_active=True)
        category_name = self.kwargs.get('cat')
        brand_name = self.kwargs.get('brand')


        if category_name is not None :
            query = query.filter(category__url_title__iexact=category_name)

        if brand_name is not None:
            query = query.filter(brand__url_title__iexact=brand_name)

        return query


    def get_context_data(self, *, object_list=None, **kwargs):
        context =super(ShopListView, self).get_context_data()
        query = Product.objects.all()
        product: Product= query.order_by('-price').first()
        db_max_price = product.price if product is not None else 0
        context['db_max_price'] = db_max_price
        context['banners'] = SiteBanner.objects.filter(is_acitve= True, position__iexact= SiteBanner.SiteBannerPositions.product_list)
        context['start_price'] = self.request.GET.get('start_price') or 0
        context['end_price'] = self.request.GET.get('end_price') or db_max_price
        return context


class ShopDetailView(DetailView):
    template_name = 'product/product-details.html'
    model = Product

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        loaded_product = self.object
        request = self.request
        context['banners'] = SiteBanner.objects.filter(is_acitve= True, position__iexact= SiteBanner.SiteBannerPositions.product_list)
        galleries = list(ProductGallery.objects.filter(product_id=loaded_product).all())
        galleries.insert(0, loaded_product)
        context['product_galleries_group'] = group_list(galleries, 3)
        context['related_products'] = group_list(list(Product.objects.filter(brand_id=loaded_product.brand_id).exclude(pk=loaded_product.id).all()[:12]), 3)


        user_ip = get_client_ip(request)
        user_id = None
        if self.request.user.is_authenticated:
            user_id = self.request.user.id
        has_been_visited = ProductVisit.objects.filter(ip__iexact= user_ip, product_id=loaded_product.id).exists()

        if not has_been_visited:
            new_visit = ProductVisit(ip=user_ip, user_id=user_id, product_id=loaded_product.id)
            new_visit.save()

        return context

def product_category_component(request: HttpRequest):
    category = ProductCategory.objects.filter(is_active=True, is_delete= False)
    context = {
        'categories': category
    }
    return render(request, 'product/component/product_category_component.html', context)


def product_brand_component(request: HttpRequest):
    brand = ProductBrand.objects.filter(is_active=True, is_delete= False).annotate(product_count=Count('product'))
    context = {
        'brands': brand
    }
    return render(request, 'product/component/product_brand_component.html', context)