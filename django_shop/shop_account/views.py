from django.http import Http404
from django.shortcuts import render, HttpResponse, redirect
from django.views import View
from django.views.generic import TemplateView

from .forms import RegisterForm, LoginForm, ForgotPass, ResetPasswordForm
from shop_Admin.models import User
from django.utils.crypto import get_random_string
from django.urls import reverse
from django.contrib.auth import login, logout, authenticate
from utils.email_service import send_email


class RegisterPageView(View):
    def get(self, request):
        form = RegisterForm()
        return render(request,'account/register.html',{
            'register_form':form
        })

    def post(self, request):
        form = RegisterForm(request.POST or None)
        context = {
            'register_form': form
        }
        if form.is_valid():
            username = form.cleaned_data.get('user_name')
            email = form.cleaned_data.get('email')
            password = form.cleaned_data.get('password')
            user_username: bool = User.objects.filter(username__iexact=username).exists()
            user_email: bool = User.objects.filter(email__iexact=email).exists()
            if user_username or user_email:
                form.add_error('email', "username or email is taken !!!")
            else:
                new_user = User(username=username,
                                email=email,
                                email_active_code=get_random_string(74),
                                is_active=False
                                )
                new_user.set_password(password)
                new_user.save()
                context['register_form']= RegisterForm()
                send_email('فعالسازی حساب کاربری', new_user.email, {'user': new_user}, 'email/active_account.html')
                return redirect(reverse('login_page'))
        return render(request, 'account/register.html', context)


class LoginPageView(View):
    def get(self, request):
        form = LoginForm
        return render(request,'account/login.html',{
            'login_form':form
        })

    def post(self, request):
        form = LoginForm(request.POST or None)

        if form.is_valid():
            username = form.cleaned_data.get('user_name')
            password = form.cleaned_data.get('password')
            user= authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect(reverse('home_page'))
            form.add_error('user_name', 'password or username is wrong!!!')


        return render(request, 'account/login.html', {
            'login_form': form
        })



class ActiveEmailCodeView(View):
    def get(self, request, activate):
        user: User = User.objects.filter(email_active_code__iexact=activate).first()
        if user is not None:
            if not user.is_active:
                user.is_active = True
                user.save()
                # todo: show success massage to user
                return redirect(reverse('login_page'))
            return render(request, 'account/confirm_account.html')
        raise Http404


class ForgetPasswordView(View):
    def get(self,request: HttpResponse):
        forget_pass_form = ForgotPass()
        context = {
            'forgot_form': forget_pass_form
        }
        return render(request, 'account/forgot_password.html', context)

    def post(self, request: HttpResponse):
        forget_pass_form = ForgotPass(request.POST or None)

        if forget_pass_form.is_valid():
            email = forget_pass_form.cleaned_data.get('email')
            user: User = User.objects.filter(email__iexact=email).first()
            if user is not None:
                send_email('فعالسازی حساب کاربری', user.email, {'user': user}, 'email/forgot_password.html')
                return redirect(reverse('login_page'))


class ResetPasswordView(View):
    def get(self, request:HttpResponse, active_code):
        user: User = User.objects.filter(email_active_code__iexact=active_code).first()
        if user is None:
            return redirect(reverse('login_page'))

        reset_pass_form = ResetPasswordForm()

        context = {
            'reset_pass':reset_pass_form,
            'user':user
        }
        return render(request, 'account/reset_password.html', context)

    def post(self, request:HttpResponse, active_code):
        reset_pass_form = ResetPasswordForm(request.POST)
        user: User = User.objects.filter(email_active_code__iexact=active_code).first()
        if reset_pass_form.is_valid():
            if user is None:
                return redirect(reverse('login_page'))
            new_pass = reset_pass_form.cleaned_data.get('password')
            user.set_password(new_pass)
            user.email_active_code = get_random_string(72)
            user.is_active = True
            user.save()
            return redirect(reverse('login_page'))
        context = {
            "rese_pass": reset_pass_form,
            'user':user
        }
        return render(request, 'account/reset_password.html', context)


class LogoutView(View):
    def get(self, request):
        logout(request)
        return redirect(reverse('home_page'))