from django import forms


class RegisterForm(forms.Form):
    user_name = forms.CharField(
        label='UserName',
        widget=forms.TextInput(attrs={
            'class':'form-control','placeholder':'username'
        })
    )
    email = forms.EmailField(
        label='Email',
        widget=forms.EmailInput(attrs={
            'class':'form-control','placeholder':'email'
        })
    )
    password = forms.CharField(
        label='Password',
        widget=forms.PasswordInput(attrs={
            'class':'form-control','placeholder':'password'
        })
    )
    confirm_pass = forms.CharField(
        label='Confirm Password',
        widget=forms.PasswordInput(attrs={
            'class':'form-control','placeholder':'confirm password'
        })
    )

    def clean_confirm_pass(self):
        password = self.cleaned_data.get('password')
        re_password = self.cleaned_data.get('confirm_pass')

        if password == re_password:
            return password
        else:
            raise forms.ValidationError('password not match !!!')

class LoginForm(forms.Form):
    user_name = forms.CharField(
        label='UserName',
        widget=forms.TextInput(attrs={
            'class':'form-control','placeholder':'username'
        })
    )
    password = forms.CharField(
        label='password',
        widget=forms.PasswordInput(attrs={
            'class':'form-control','placeholder':'password'
        })
    )


class ForgotPass(forms.Form):
    email = forms.EmailField(
        label='Email',
        widget=forms.EmailInput(attrs={
            'class': 'form-control', 'placeholder': 'email'
        })
    )

class ResetPasswordForm(forms.Form):
    password = forms.CharField(
        label='Password',
        widget=forms.PasswordInput(attrs={
            'class': 'form-control', 'placeholder': 'password'
        })
    )
    confirm_pass = forms.CharField(
        label='Confirm Password',
        widget=forms.PasswordInput(attrs={
            'class': 'form-control', 'placeholder': 'confirm password'
        })
    )