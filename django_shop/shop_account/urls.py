from django.urls import path
from .views import RegisterPageView, LoginPageView, ActiveEmailCodeView, ResetPasswordView, ForgetPasswordView, LogoutView


urlpatterns = [
    path('register', RegisterPageView.as_view(), name='register_page'),
    path('login', LoginPageView.as_view(), name='login_page'),
    path('logout', LogoutView.as_view(), name='logout_page'),
    path('active/<activate>', ActiveEmailCodeView.as_view(), name='activate'),
    path('forgot-password', ForgetPasswordView.as_view(), name='forgot_pass'),
    path('reset-password/<active_code>', ResetPasswordView.as_view(), name='reset_password_page'),

]
