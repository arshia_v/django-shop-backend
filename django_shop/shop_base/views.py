from django.shortcuts import render
from django.views.generic import TemplateView
from shop_product.models import Product
from shop_site_setting.models import SiteSettings, Slider, FooterLinkBox
from utils.convertors import group_list
from django.db.models import Count
from shop_product.models import ProductCategory
class HomePageView(TemplateView):
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        context = super(HomePageView, self).get_context_data(**kwargs)
        slider_setting: Slider =Slider.objects.filter(slider_active=True)
        context['sliders']= slider_setting
        newest_products = Product.objects.filter(is_active=True, is_delete=False).order_by('-id')[:12]
        most_visit_products = Product.objects.filter(is_active=True, is_delete=False).annotate(visit_count=Count('productvisit')).order_by('-visit_count')[:12]
        
        context['newest_products'] = group_list(newest_products)
        context['most_visit_products'] = group_list(most_visit_products)

        categories = list(ProductCategory.objects.annotate(product_count=Count('product_categories')).filter(is_active=True, is_delete=False, product_count__gt=0)[:6])
        categories_products = []
        for category in categories:
            item = {
                'id': category.id,
                'title': category.title,
                'product': list(category.product_categories.all()[:4])
            }
            categories_products.append(item)

        context['categories_products']= categories_products

        return context

class AboutUsView(TemplateView):
    template_name = 'About.html'

    def get_context_data(self, **kwargs):
        context = super(AboutUsView, self).get_context_data(**kwargs)
        setting: SiteSettings = SiteSettings.objects.filter(main_site_setting=True).first()
        context['site_setting'] = setting
        return context

def site_header_component(request):
    settings: SiteSettings = SiteSettings.objects.filter(main_site_setting=True).first()
    context = {
        'site_setting':settings
    }
    return render(request, 'layout/_header.html', context)


def site_footer_component(request):
    settings: SiteSettings =SiteSettings.objects.filter(main_site_setting=True).first()
    footer_link_boxs = FooterLinkBox.objects.all()
    context = {
        'site_setting':settings,
        'footer_link_boxs':footer_link_boxs
    }
    return render(request, 'layout/_footer.html', context)