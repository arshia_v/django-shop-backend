from django.http import HttpRequest, JsonResponse
from shop_product.models import Product
from .models import Order, OrderDetail

# Create your views here.


def add_product_to_order(request: HttpRequest):
    product_id = int(request.GET.get('product_id'))
    count = int(request.GET.get('count'))
    if count < 1:
            return JsonResponse({
                'status': 'invalid_count',
                'text':'مقدار وارد شده معتبر نیست!',
                'confirm_button_text':'ببخشید',
                'icon':'warning'
            })

    if request.user.is_authenticated:
        product = Product.objects.filter(id=product_id, is_active=True, is_delete=False).first()
        if product is not None:
            current_order, created = Order.objects.get_or_create(is_paid=False, user_id=request.user.id)
            current_order_detail = current_order.orderdetail_set.filter(product_id=product_id).first()
            if current_order_detail is not None:
                current_order_detail.count += count
                current_order_detail.save()
            else:
                new_order = OrderDetail(order_id=current_order.id, product_id=product_id, count=count)
                new_order.save()

            return JsonResponse({
                'status':'success',
                'text': 'محصول مورد نظر باموفقیت ثبت شد',
                'confirm_button_text': 'مرسی',
                'icon': 'success'
            })
        else:
            return JsonResponse({
                'status': 'not_found',
                'text': 'محصول مورد نظر یافت نشد',
                'confirm_button_text': 'مرسی',
                'icon': 'error'
            })

    else:
        return JsonResponse({
            'status':'not_auth',
            'text': 'برای افزودن محصول به سبد خرید باید اول وارد سایت شوید',
            'confirm_button_text': 'مرسی',
            'icon': 'error'
        })


