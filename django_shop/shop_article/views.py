from django.http import HttpRequest, HttpResponse
from django.shortcuts import render
from django.views.generic import DetailView
from django.views.generic.list import ListView
from shop_article.models import Article, ArticleCategory, ArticleComment


class ArticleListView(ListView):
    model = Article
    template_name = 'article_shop/article.html'
    paginate_by = 4

    def get_context_data(self, *args, **kwargs):
        context = super(ArticleListView, self).get_context_data(*args, **kwargs)
        return context

    def get_queryset(self):
        query = super(ArticleListView, self).get_queryset()
        query = query.filter(is_active=True)
        category_name = self.kwargs.get('category')
        if category_name is not None :
            query = query.filter(selected_category__url_title__iexact=category_name)
        return query


class ArticleDetailView(DetailView) :
    model = Article
    template_name = 'article_shop/article_detail.html'

    def get_queryset(self):
        query = super(ArticleDetailView, self).get_queryset()
        id = self.kwargs.get('pk')
        query = query.filter(is_active=True)
        if query is not None:
            query = query.filter(id=id)
        return query

    def get_context_data(self, **kwargs):
        context = super(ArticleDetailView, self).get_context_data(**kwargs)
        article: Article = kwargs.get('object')
        context['comments'] = ArticleComment.objects.filter(article_id= article.id, parent=None).order_by('-create_date').prefetch_related('articlecomment_set')
        return context

def category_article(request):
    main_category : ArticleCategory = ArticleCategory.objects.filter(is_active=True, parent=None)
    context = {
        'main_categories': main_category
    }
    return render(request, 'article_shop/article_component.html', context)


def add_article_comment(request: HttpRequest):
    if request.user.is_authenticated:
        article_id = request.GET.get('article_id')
        article_comment = request.GET.get('article_comment')
        parent_id = request.GET.get('parent_id')
        print(article_comment, article_id, parent_id)
        new_comment = ArticleComment(article_id=article_id, text=article_comment, user_id=request.user.id, parent_id=parent_id)
        new_comment.save()
        context = {
            'comments': ArticleComment.objects.filter(article_id= article_id, parent=None).order_by('-create_date').prefetch_related('articlecomment_set'),
            'comments_count': ArticleComment.objects.filter(article_id= article_id).count()
        }
        return render(request, 'article_shop/includes/article_comments_partial.html', context)

    return HttpResponse('response')




