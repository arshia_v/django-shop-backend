from django.db import models

from shop_Admin.models import User


class ArticleCategory(models.Model):
    title = models.CharField(max_length=150 )
    url_title = models.CharField(max_length=200, unique=True)
    parent = models.ForeignKey(to="ArticleCategory", on_delete=models.CASCADE, null=True, blank=True)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.title



class Article(models.Model):
    title = models.CharField(max_length=200)
    slug = models.SlugField(max_length=200, db_index=True, allow_unicode=True)
    short_description = models.TextField()
    description = models.TextField()
    selected_category = models.ManyToManyField(to='ArticleCategory', )
    image = models.ImageField(upload_to='images/articles')
    is_active = models.BooleanField(default=False)
    author = models.ForeignKey(User, on_delete=models.CASCADE, null=True, editable=False)
    add_time = models.DateTimeField(auto_now_add=True, editable=True)

    def __str__(self):
        return self.title


class ArticleComment(models.Model):
    article = models.ForeignKey(Article, on_delete=models.CASCADE, )
    parent = models.ForeignKey("ArticleComment", on_delete=models.CASCADE, null=True, blank=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    create_date = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    text = models.TextField(null=True, blank=True)

    def __str__(self):
        return str(self.user)
        # return f'user : {self.user} || message for this article : {self.article}'