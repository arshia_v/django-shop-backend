from django.urls import path
from .views import ArticleListView, ArticleDetailView, add_article_comment

urlpatterns = [
    path('', ArticleListView.as_view(), name= 'article_page'),
    path('cat/<str:category>', ArticleListView.as_view(), name= 'article_category_page'),
path('add-article-comment', add_article_comment , name='add_article_comment_page'),
    path('<pk>', ArticleDetailView.as_view(), name= 'article_detail_page'),
]
