from django import forms



class ContactForm(forms.Form):
    email = forms.EmailField(
        widget=forms.EmailInput(attrs={
            'class':'form-control', 'place_holder':'email'
        })
    )
    user_name = forms.CharField(
        widget=forms.TextInput(attrs={
            'class':'form-control', 'place_holder':'username'
        })
    )
    subject = forms.CharField(
        widget=forms.TextInput(attrs={
            'class':'form-control', 'place_holder':'subject'
        })
    )
    message = forms.CharField(
        widget=forms.Textarea(attrs={
            'class':'form-control', 'place_holder':'your message'
        })
    )