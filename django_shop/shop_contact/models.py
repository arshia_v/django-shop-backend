from django.db import models

# Create your models here.

class Contact(models.Model):
    user_name = models.CharField(max_length=200)
    subject = models.CharField(max_length=200)
    email = models.EmailField(max_length=200)
    message = models.TextField()
    is_read = models.BooleanField(default=False)

    def __str__(self):
        return self.subject