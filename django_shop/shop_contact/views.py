from django.http import Http404
from django.shortcuts import render, redirect
from django.views.generic import TemplateView, View
from .models import Contact
from .forms import ContactForm
from shop_site_setting.models import SiteSettings


class ContactUs(View):
    def get(self, request):
        setting: SiteSettings =SiteSettings.objects.filter(main_site_setting=True).first()
        form = ContactForm()
        context = {
            'contact_form':form,
            'site_setting': setting
        }
        return render(request, 'contact/contact-us.html', context)

    def post(self, request):
        form = ContactForm(request.POST or None)
        context = {
            'contact_form':form
        }
        if form.is_valid():
            userName = form.cleaned_data.get('user_name')
            email = form.cleaned_data.get('email')
            subject = form.cleaned_data.get('subject')
            message = form.cleaned_data.get('message')
            new_contact = Contact(user_name=userName, subject=subject, email=email, message=message)
            new_contact.save()
            context['contact_form'] = ContactForm
        else:
            raise Http404
        return render(request, 'contact/contact-us.html', context)