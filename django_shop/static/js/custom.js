function sendArticleComment(articleId){
    var comment = $('#commentText').val();
    var parentId = $('#parent_id').val();
    $.get('/article/add-article-comment', {
        article_comment : comment,
        article_id: articleId,
        parent_id: parentId
    }).then(res => {
        console.log(res);
        //location.reload();
        $('#comments_area').html(res);
    });
}

function fillParentId(parentId) {
    $('#parent_id').val(parentId);
    document.getElementById('comment_form').scrollIntoView({behavior: 'smooth'})
}

function showLargeImage(imageSrc){
    $('#main_image').attr('src', imageSrc);
    $('#show_larg_image').attr('href', imageSrc);
}



function addProductToOrder(productId){
    const productCount = $('#product-count').val();
    $.get('/order/add-to-order?product_id=' + productId + '&count=' + productCount).then(res => {
        Swal.fire({
            title: 'اعلان',
            text: res.text,
            icon: res.icon,
            showCancelButton: false,
            confirmButtonColor: '#3085d6',
            confirmButtonText: res.confirm_button_text
        }).then((result) => {
            if (result.isConfirmed && res.status === 'not_auth') {
                window.location.href = '/account/login';
            }
        })

    });
}

function removeOrderDetail(detailId){
    $.get('/user-panel/remove-order-detail?detail_id=' + detailId).then(res => {
        if(res.status === 'success'){
            $('#order-detail-content').html(res.body);
        }
    });
}

function changeOrderDetailCount(detailId, state) {
    $.get('/user-panel/change-order-detail?detail_id=' + detailId + '&state=' + state).then(res => {
        if (res.status === 'success') {
            $('#order-detail-content').html(res.body);
        }
    });
}
